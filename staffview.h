#ifndef SEQUENCE_STAFFVIEW_H
#define SEQUENCE_STAFFVIEW_H

#include <QAbstractScrollArea>
#include <QPen>
#include <QPair>
class NotePainter;
class MidiFile;
class MidiNote;

class StaffView : public QAbstractScrollArea
{
Q_OBJECT
public:
  StaffView(QWidget* parent = nullptr);
  StaffView(MidiFile* file, QWidget* parent = nullptr);

  void setScale(double value);
  double scale() const;

protected:
  void paintEvent(QPaintEvent* event);
  void renderBar(double scrollX, int index, bool hidden = false);
  double renderNote(NotePainter* np, MidiNote* note, int index, double x);
  int32_t beatNumber(MidiNote* note) const;
  QPair<uint32_t, uint8_t> noteType(int duration) const;

private:
  QPen selectionPen;
  double scaleFactor;
  double pointsPerPixel;
  bool inBeam;
  MidiFile* midiFile;

  struct BarLayout {
    double x;
    uint32_t startTime;
  };
  QVector<BarLayout> barLayout;
};

#endif
