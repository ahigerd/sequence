#ifndef SEQUENCE_PIANOROLLVIEW_H
#define SEQUENCE_PIANOROLLVIEW_H

#include <QAbstractScrollArea>
class MidiMTrk;
class QPaintEvent;

class PianoRollView : public QAbstractScrollArea
{
  Q_OBJECT
public:
  PianoRollView(QWidget* parent = 0, MidiMTrk* trk = 0, int16_t resolution = 0);
  ~PianoRollView();

  MidiMTrk* track;

  uint16_t getDivision() const;
  int8_t getFPS() const;
  uint8_t getResolution() const;
  void setDivision(uint16_t ppqn);
  void setDivision(int8_t fps, uint8_t resolution);

  uint16_t zoom() const;
  void setZoom(uint16_t level);

protected:
  void paintEvent(QPaintEvent* event);
  void resizeEvent(QResizeEvent* event);

  uint16_t res, zoomlevel;
  uint8_t numBeats, beatNote;
  bool smtpe;
};

#endif
