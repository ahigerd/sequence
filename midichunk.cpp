#include <QLinkedList>
#include "midichunk.h"
#include "midievent.h"
#include <QDebug>

MidiChunk::~MidiChunk() {}

QByteArray MidiChunk::chunk() const
{
  return id() + longToBA(data().length()) + data();
}

MidiMThd::MidiMThd(uint16_t fmt, uint16_t trks, uint16_t div)
{
  format = fmt;
  tracks = trks;
  setDivision(div);
}

MidiMThd::MidiMThd(uint16_t fmt, uint16_t trks, int8_t fps, uint8_t resolution)
{
  format = fmt;
  tracks = trks;
  setDivision(fps, resolution);
}

MidiMThd::MidiMThd(const QByteArray& src)
{
  format = (uint16_t)(src[9]);
  tracks = baToShort(src.mid(10, 2));
  setDivision(-src[12], src[13]);
}

uint16_t MidiMThd::getDivision() const
{
  return division;
}

int8_t MidiMThd::getFPS() const
{
  return -((division & 0xFF00) >> 8);
}

uint8_t MidiMThd::getResolution() const
{
  return (division & 0xFF);
}

void MidiMThd::setDivision(uint16_t ppqn)
{
  division = ppqn;
}

void MidiMThd::setDivision(int8_t fps, uint8_t resolution)
{
  division = -(fps * 256) + resolution;
}

QByteArray MidiMThd::data() const
{
  return shortToBA(format) + shortToBA(tracks) + shortToBA(division);
}

MidiMTrk::MidiMTrk()
{
  // initializers only
}

static inline int16_t read16(const QByteArray& src, uint32_t& i) {
  uint8_t high = src[++i];
  uint8_t low = src[++i];
  return (high << 8) | low;
}

MidiMTrk::MidiMTrk(const QByteArray& src)
{
  QLinkedList<MidiNote*> sustain;
  QLinkedList<MidiNote*>::iterator j;
  uint32_t len = baToLong(src.mid(4, 4)) + 8;
  uint32_t abstime = 0, time = 0;
  uint8_t msg = 0, chan = 0, meta = 0;
  MidiEvent* ev;
  qDebug() << "Start track: length" << len;
  for (uint32_t i = 8; i < len; ++i) {
    time = fromVarLen(src.mid(i));
    abstime += time;
    if (time >= 0x200000) {
      ++i;
    }
    if (time >= 0x4000) {
      ++i;
    }
    if (time >= 0x80) {
      ++i;
    }
    ++i;
    if (src[i] & 0x80) {
      msg = src[i] & 0xF0;
      chan = src[i] & 0x0F;
    } else {
      --i;
    }
    switch (msg) {
    case 0x80:
      for (j = sustain.begin(); j != sustain.end();) {
        if (uint8_t(src[i + 1]) == (*j)->note) {
          (*j)->length = abstime - (*j)->offset;
          (*j)->release = src[i + 2];
          j = sustain.erase(j);
        } else {
          j++;
        }
      }
      i += 2;
      continue;
    case 0x90:
      if (uint8_t(src[i + 2]) != 0) {
        ev = new MidiNote(chan, src[++i], abstime, src[++i]);
        sustain.append(static_cast<MidiNote*>(ev));
      } else {
        for (j = sustain.begin(); j != sustain.end();) {
          if (uint8_t(src[i + 1]) == (*j)->note) {
            (*j)->length = abstime - (*j)->offset;
            (*j)->release = 0;
            j = sustain.erase(j);
          } else {
            j++;
          }
        }
        i += 2;
        continue;
      }
      break;
    case 0xA0:
      ev = new MidiAftertouch(chan, src[++i], src[++i]);
      break;
    case 0xB0:
      ev = new MidiController(chan, src[++i], int16_t(src[++i]));
      break;
    case 0xC0:
      ev = new MidiProgChange(chan, src[++i]);
      break;
    case 0xD0:
      ev = new MidiPressure(chan, src[++i]);
      break;
    case 0xE0:
      ev = new MidiPitch(chan, read16(src, i));
      break;
    case 0xF0:
      if (chan == 15) {
        meta = src[++i];
        if (meta == 0x2F) {
          return;  // end-of-track marker
        }
      }
      int sysexlen = fromVarLen(src.mid(++i));
      if (sysexlen >= 0x200000) {
        ++i;
      }
      if (sysexlen >= 0x4000) {
        ++i;
      }
      if (sysexlen >= 0x80) {
        ++i;
      }
      ++i;
      if (chan == 15) {
        ev = new MidiMeta(meta, src.mid(i, sysexlen));
      } else {
        ev = new MidiSysex(chan, src.mid(i, sysexlen));
      }
      i += sysexlen - 1;
    }
    ev->offset = abstime;
    events.append(ev);
  }
}

MidiMTrk::~MidiMTrk()
{
  MidiEvent* t;
  while (!events.isEmpty()) {
    t = events.takeFirst();
    if (t) {
      delete t;
    }
  }
}


QByteArray MidiMTrk::data() const
{
  QLinkedList<MidiNote*> sustain;
  QLinkedList<MidiNote*>::iterator j;
  QByteArray retval;
  int len = events.count(), i;
  uint32_t pos = 0, offpos = 0;
  uint8_t running = 0;
  for (i = 0; i < len; ++i) {
    for (j = sustain.begin(); j != sustain.end();) {
      offpos = (*j)->offset + (*j)->length;
      if (offpos > events[i]->offset) {
        j++;
        continue;
      }
      retval += toVarLen(offpos - pos);
      if ((*j)->status() == running && (*j)->release == 0) {
        retval += QByteArray(1, (*j)->note) + QByteArray(1, 0);
      } else if ((0x80 | (*j)->channel) == running) {
        retval += QByteArray(1, (*j)->note) + QByteArray(1, (*j)->release);
      } else {
        running = 0x80 | (*j)->channel;
        retval += QByteArray(1, running) + QByteArray(1, (*j)->note) + QByteArray(1, (*j)->release);
      }
      j = sustain.erase(j);
      pos = offpos;
    }
    retval += toVarLen(events[i]->offset - pos);
    pos = events[i]->offset;
    if (events[i]->message() != 0xF0 && i != 0 && events[i]->status() == running) {
      retval += events[i]->data();
    } else {
      retval += events[i]->event();
    }
    if (events[i]->message() == 0x90) {
      sustain.append(static_cast<MidiNote*>(events[i]));
    }
    running = events[i]->status();
  }
  while (!sustain.empty()) {
    offpos = 0;
    for (j = sustain.begin(); j != sustain.end(); j++)
      if ((*j)->offset + (*j)->length > offpos) {
        offpos = (*j)->offset + (*j)->length;
      }
    for (j = sustain.begin(); j != sustain.end();) {
      // Loop content copy-pasted from above
      // Watch out when modifying
      if ((*j)->offset + (*j)->length != offpos) {
        j++;
        continue;
      }
      retval += toVarLen(offpos - pos);
      if ((*j)->status() == running && (*j)->release == 0) {
        retval += QByteArray(1, (*j)->note) + QByteArray(1, 0);
      } else if ((0x80 | (*j)->channel) == running) {
        retval += QByteArray(1, (*j)->note) + QByteArray(1, (*j)->release);
      } else {
        running = 0x80 | (*j)->channel;
        retval += QByteArray(1, running) + QByteArray(1, (*j)->note) + QByteArray(1, (*j)->release);
      }
      j = sustain.erase(j);
      pos = offpos;
    }
  }
  MidiMeta eot(0x2F, "");
  retval += uint8_t(0);
  retval += eot.event();
  return retval;
}

