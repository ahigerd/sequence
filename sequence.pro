TARGET = sequence
TEMPLATE = app
DEPENDPATH += .
INCLUDEPATH += .
OBJECTS_DIR = .tmp
MOC_DIR = .tmp
RCC_DIR = .tmp
QT += widgets
CONFIG += debug

HEADERS += midichunk.h   midievent.h   midifile.h   pianorollview.h
SOURCES += midichunk.cpp midievent.cpp midifile.cpp pianorollview.cpp

HEADERS += notepaths.h   notepainter.h   staffview.h
SOURCES += notepaths.cpp notepainter.cpp staffview.cpp

SOURCES += main.cpp
