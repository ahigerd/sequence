#include "midievent.h"

MidiEvent::MidiEvent(uint8_t chan, uint32_t time)
{
  channel = chan;
  offset = time;
}

MidiEvent::~MidiEvent() {}

uint8_t MidiEvent::status() const
{
  return channel | message();
}

QByteArray MidiEvent::event() const
{
  return QByteArray(1, status()) + data();
}

MidiNote::MidiNote(uint8_t chan, uint8_t noteval, uint32_t time, uint8_t atk, uint32_t len, uint8_t rel)
: MidiEventBase<MidiNote, MidiNote::Message>(chan, time)
{
  note = noteval;
  attack = atk;
  length = len;
  release = rel;
}

QByteArray MidiNote::data() const
{
  return QByteArray(1, note).append(attack);
}

MidiProgChange::MidiProgChange(uint8_t chan, uint8_t prog, uint32_t time)
: MidiEventBase<MidiProgChange, MidiProgChange::Message>(chan, time)
{
  program = prog;
}

QByteArray MidiProgChange::data() const
{
  return QByteArray(1, program);
}

MidiController::MidiController(uint8_t chan, uint8_t ctrl, int16_t coarseval, int8_t fineval, uint32_t time)
: MidiEventBase<MidiController, MidiController::Message>(chan, time)
{
  if (ctrl == mcNonReg + mcFineOffset) {
    ctrl = mcNonRegFine;
  }
  if (ctrl == mcReg + mcFineOffset) {
    ctrl = mcRegFine;
  }
  controller = ctrl;
  fine = fineval;
  if (coarseval > 255) {
    fine = (coarseval & 0xFF00) >> 8;
  }
  coarse = (coarseval & 0x00FF);
}

QByteArray MidiController::data() const
{
  QByteArray retval(1, controller);
  retval += coarse;
  if (fine != -1) {
    MidiController fineEvent(channel, (uint8_t)(controller + mcFineOffset), fine);
    retval += fineEvent.event();
  }
  return retval;
}

int16_t MidiController::value() const
{
  if (fine == -1) {
    return coarse;
  }
  return coarse + fine * 256;
}

void MidiController::setValue(int16_t value)
{
  fine = (value & 0xFF00) >> 8;
  coarse = (value & 0x00FF);
}

MidiAftertouch::MidiAftertouch(uint8_t chan, uint8_t noteval, uint8_t press, uint32_t time)
: MidiEventBase<MidiAftertouch, MidiAftertouch::Message>(chan, time)
{
  note = noteval;
  pressure = press;
}

QByteArray MidiAftertouch::data() const
{
  return QByteArray(1, note) + QByteArray(1, pressure);
}

MidiPressure::MidiPressure(uint8_t chan, uint8_t press, uint32_t time)
: MidiEventBase<MidiPressure, MidiPressure::Message>(chan, time)
{
  pressure = press;
}

QByteArray MidiPressure::data() const
{
  return QByteArray(1, pressure);
}

MidiPitch::MidiPitch(uint8_t chan, uint16_t val, uint32_t time)
: MidiEventBase<MidiPitch, MidiPitch::Message>(chan, time)
{
  value = val;
}

QByteArray MidiPitch::data() const
{
  return shortToBA(value);
}

MidiSysex::MidiSysex(uint8_t chan, const QByteArray& stuff, uint32_t time)
: MidiEventBase<MidiSysex, MidiSysex::Message>(chan, time)
{
  content = stuff;
}

QByteArray MidiSysex::data() const
{
  if (channel == 15) {
    return content[0] + toVarLen(content.length() - 1) + content.mid(1);
  } else {
    return toVarLen(content.length()) + content;
  }
}

MidiMeta::MidiMeta(uint8_t evtype, const QByteArray& stuff, uint32_t time) : MidiSysex(15, "", time)
{
  content = stuff;
  content.prepend(evtype);
}

uint8_t MidiMeta::eventType() const
{
  return content[0];
}

MidiText::MidiText(uint8_t evtype, const QString& text, uint32_t time) : MidiMeta(evtype, text.toUtf8(), time)
{
  // initializers only
}

QString MidiText::text() const
{
  return QString::fromUtf8(content.mid(1));
}

void MidiText::setText(const QString& value)
{
  content = content[0] + value.toUtf8();
}

MidiChannelPrefix::MidiChannelPrefix(uint8_t channel, uint32_t time) : MidiMeta(MidiMeta::ChannelPrefix, QByteArray(1, channel), time)
{
  // initializers only
}

uint8_t MidiChannelPrefix::channel() const
{
  return content[1];
}

void MidiChannelPrefix::setChannel(uint8_t channel)
{
  content[1] = channel;
}

MidiSetPort::MidiSetPort(uint8_t port, uint32_t time) : MidiMeta(MidiMeta::SetPort, QByteArray(1, port), time)
{
  // initializers only
}

uint8_t MidiSetPort::port() const
{
  return content[1];
}

void MidiSetPort::setPort(uint8_t port)
{
  content[1] = port;
}

MidiTempo::MidiTempo(uint32_t tempo, uint32_t time) : MidiMeta(MidiMeta::Tempo, longToBA(tempo).mid(1), time)
{
  // initializers only
}

uint32_t MidiTempo::tempo() const
{
  return baToLong(content) & 0x00FFFFFF;
}

void MidiTempo::setTempo(uint32_t tempo)
{
  content = content[0] + longToBA(tempo).mid(1);
}

MidiTimeSignature::MidiTimeSignature(uint8_t beats, uint8_t beatType, uint8_t metronome, uint8_t quarter32nds, uint32_t time)
: MidiMeta(MidiMeta::TimeSignature, QByteArray(4, '\0'), time)
{
  setTimeSignature(beats, beatType);
  if (metronome != 0xFF) {
    setMetronome(metronome);
  }
  setQuarter32nds(quarter32nds);
}

uint8_t MidiTimeSignature::beats() const
{
  return content[1];
}

uint8_t MidiTimeSignature::beatType() const
{
  return content[2];
}

void MidiTimeSignature::setTimeSignature(uint8_t beats, uint8_t beatType, uint8_t beatsPerClick)
{
  content[1] = beats;
  content[2] = beatType;
  setMetronome((96 >> beatType) * beatsPerClick);
}

uint8_t MidiTimeSignature::metronome() const
{
  return content[3];
}

uint8_t MidiTimeSignature::beatsPerClick() const
{
  return content[3] / (96 >> content[2]);
}

void MidiTimeSignature::setMetronome(uint8_t clocksPerClick)
{
  content[3] = clocksPerClick;
}

uint8_t MidiTimeSignature::quarter32nds() const
{
  return content[4];
}

void MidiTimeSignature::setQuarter32nds(uint8_t count)
{
  content[4] = count;
}
