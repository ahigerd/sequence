#include "notepaths.h"
#include <QMap>
#include <cmath>

static const double CURVE = 0.5522847498;
static const double STEM_X = 0.556;
static const double STEM_Y = -0.15;
static const double STEM_W = -0.1;
static const double STEM_H = 3.35;
static const double W_RX = 0.46695;
static const double W_RY = 0.27000;
static const double W_THETA = 0.0;
static const double WH_RX = 0.21000;
static const double WH_RY = 0.23920;
static const double WH_THETA = -1.0;
static const double H_RX = 0.34000;
static const double H_RY = 0.22500;
static const double H_THETA = -0.4;
static const double HH_RX = 0.29580;
static const double HH_RY = 0.13500;
static const double HH_THETA = -0.5;

static inline QPointF rotatePoint(double x, double y, double theta)
{
  double ct = std::cos(theta);
  double st = std::sin(theta);
  return QPointF(x * ct - y * st, y * ct + x * st);
}

static QPainterPath makeTrebleClef()
{
  QPainterPath path;
  path.translate(0, 0.05);
  path.moveTo(0.00000, 0.92700);
  path.cubicTo(-0.03904, 1.13543, 0.17841, 1.56828, 0.61590, 1.58242);
  path.cubicTo(1.14480, 1.59952, 1.48270, 1.19494, 1.32527, 0.53466);
  path.lineTo(0.53579, -3.10890);
  path.cubicTo(0.44181, -3.54263, 0.51235, -3.77789, 0.56254, -3.91809);
  path.cubicTo(0.69642, -4.29213, 1.03090, -4.44641, 1.11234, -4.32666);
  path.cubicTo(1.15883, -4.25829, 1.27124, -3.93861, 0.95124, -3.36603);
  path.cubicTo(0.70167, -2.91946, -0.45951, -2.33388, -0.54472, -1.28594);
  path.cubicTo(-0.61849, -0.37466, 0.33544, 0.29837, 1.15878, 0.03378);
  path.cubicTo(1.70541, -0.14188, 1.98721, -0.97150, 1.64886, -1.39943);
  path.cubicTo(0.75596, -2.52874, -0.67604, -0.80040, 0.71115, -0.32691);
  path.cubicTo(0.56031, -0.44559, 0.45749, -0.53749, 0.43835, -0.64981);
  path.cubicTo(0.25196, -1.74337, 1.89626, -1.45465, 1.45322, -0.41720);
  path.cubicTo(1.27104, 0.00940, 0.53518, 0.09193, 0.11611, -0.28866);
  path.cubicTo(-0.23495, -0.60748, -0.39072, -1.22166, -0.00006, -1.66257);
  path.cubicTo(0.52905, -2.25975, 1.15375, -2.79584, 1.31967, -3.61333);
  path.cubicTo(1.51587, -4.58004, 0.87528, -5.33370, 0.47028, -4.38279);
  path.cubicTo(0.33439, -4.06372, 0.28377, -3.74341, 0.41389, -3.12962);
  path.lineTo(1.20275, 0.59148);
  path.cubicTo(1.25922, 0.85789, 1.22141, 1.22448, 0.97277, 1.39297);
  path.cubicTo(0.84961, 1.47643, 0.41377, 1.53387, 0.30714, 1.33464);
  path.arcTo(.34 - .34434, .92 - .35024, .68868, .70048, 240, 300);
  path.closeSubpath();
  path.translate(0, -0.05);
  path.setFillRule(Qt::WindingFill);
  return path;
}

static QPainterPath makeNoteDot()
{
  QPainterPath path;
  // TODO
  return path;
}

static inline double stemLengthUnits(int noteType)
{
  return -(STEM_Y - STEM_H - (noteType > 4 ? noteType - 4 : 0));
}

static void addRotatedEllipse(QPainterPath& path, double rx, double ry, double theta, bool reverse = false)
{
  rx *= 2;
  ry *= 2;
  QPointF minor = rotatePoint(rx, 0, theta), major = rotatePoint(0, ry, theta);
  QPointF c1 = rotatePoint(rx, ry * CURVE, theta), c2 = rotatePoint(rx * CURVE, ry, theta);
  QPointF c3 = rotatePoint(-rx * CURVE, ry, theta), c4 = rotatePoint(-rx, ry * CURVE, theta);
  if (!reverse) {
    path.moveTo(minor);
    path.cubicTo(c1, c2, major);
    path.cubicTo(c3, c4, -minor);
    path.cubicTo(-c1, -c2, -major);
    path.cubicTo(-c3, -c4, minor);
  } else {
    path.moveTo(minor);
    path.cubicTo(-c4, -c3, -major);
    path.cubicTo(-c2, -c1, -minor);
    path.cubicTo(c4, c3, major);
    path.cubicTo(c2, c1, minor);
  }
}

static void addStem(QPainterPath& path, bool down, double stemLength)
{
  if (down) {
    path.addRect(QRectF(-STEM_X - .001, -STEM_Y, STEM_W, stemLength + STEM_Y));
  } else {
    path.addRect(QRectF(STEM_X - STEM_W, STEM_Y, STEM_W, -stemLength - STEM_Y));
  }
}

static void addFlag(QPainterPath& path, bool down, int noteType)
{
  double dy = down ? -1 : 1;
  double bx = down ? -STEM_X : STEM_X - STEM_W;
  double by = dy * (STEM_Y - STEM_H - (noteType > 4 ? noteType - 4 : 3 - noteType));
  path.moveTo(bx, by);
  path.cubicTo(bx + -0.07, by + dy * 0.675, bx + 1.554, by + dy * 0.72, bx + 0.84, by + dy * 2.475);
  path.lineTo(bx + 0.77, by + dy * 2.355);
  path.cubicTo(bx + 1.12, by + dy * 1.35, bx + 0.56, by + dy * 1.125, bx, by + dy * 0.765);
  path.lineTo(bx, by);
}

static QMap<int, QPainterPath> notePaths;
static QMap<int, QPainterPath> headPaths;
static QMap<double, QPainterPath> flaglessNotePaths;

namespace NotePaths
{

QPainterPath trebleClef = makeTrebleClef();
QPainterPath noteDot = makeNoteDot();

QPainterPath head(int headType, bool down, bool cluster)
{
  int key = (cluster ? 1000 : 0) + (down ? -headType : headType);
  QPainterPath path = headPaths[key];
  if (path.isEmpty()) {
    path.setFillRule(Qt::WindingFill);
    double offset = 0;
    if (cluster) {
      offset = -4 * (headType == 0 ? W_RX - 0.08 : H_RX - 0.04);
      path.translate(offset, 0);
    }
    switch (headType) {
    case 0:
      addRotatedEllipse(path, W_RX, W_RY, W_THETA, down);
      addRotatedEllipse(path, WH_RX, WH_RY, WH_THETA, !down);
      break;
    case 1:
      addRotatedEllipse(path, HH_RX, HH_RY, HH_THETA, !down);
      // fallthrough
    default:
      addRotatedEllipse(path, H_RX, H_RY, H_THETA, down);
    }
    if (offset) {
      path.translate(-offset, 0);
    }
    headPaths[key] = path;
  }
  return path;
}

QPainterPath flagless(int headType, double length, bool down)
{
  double key = headType + 1000 * (down ? -length : length);
  QPainterPath path = flaglessNotePaths[key];
  if (path.isEmpty()) {
    path = head(headType, down, false);
    if (headType != 0) {
      addStem(path, down, length);
    }
    flaglessNotePaths[key] = path;
  }
  return path;
}

QPainterPath note(int noteType, bool down)
{
  QPainterPath path = notePaths[down ? -noteType : noteType];
  if (path.isEmpty()) {
    path = flagless(noteType, stemLengthUnits(noteType), down);
    for (int i = 3; i <= noteType; i++) {
      addFlag(path, down, i);
    }
    notePaths[down ? -noteType : noteType] = path;
  }
  return path;
}

}

double DrawableNote::stemLength() const
{
  return stemLengthUnits(noteType);
}

double DrawableNote::stemWidth() const
{
  return -STEM_W;
}

QPointF DrawableNote::stemTip(int extend) const
{
  double length = extend > noteType ? stemLengthUnits(extend) : stemLength();
  if (stemDown) {
    return QPointF(headPos.x() - STEM_X + STEM_W, headPos.y() + length);
  } else {
    return QPointF(headPos.x() + STEM_X, headPos.y() - length);
  }
}
