#include <QByteArray>
#include <QIODevice>
#include "midifile.h"
#include "midichunk.h"
#include "midievent.h"

MidiFile::MidiFile(uint16_t format, uint16_t division)
{
  header = new MidiMThd(format, 0, division);
}

MidiFile::MidiFile(uint16_t format, int8_t fps, uint8_t resolution)
{
  header = new MidiMThd(format, 0, fps, resolution);
}

MidiFile::MidiFile(const QByteArray& src)
{
  load(src);
}

MidiFile::MidiFile(QIODevice* src)
{
  bool mustClose = false;
  if (!src->isOpen()) {
    mustClose = true;
    src->open(QIODevice::ReadOnly);
  }
  load(src->readAll());
  if (mustClose) {
    src->close();
  }
}

MidiFile::~MidiFile()
{
  MidiMTrk* t;
  delete header;
  while (!tracks.isEmpty()) {
    t = tracks.takeFirst();
    if (t) {
      delete t;
    }
  }
}

void MidiFile::load(const QByteArray& src)
{
  int i, pos, len = src.length();
  QByteArray trk;
  for (pos = 0; pos < len; pos++) {
    if (src.mid(pos, 4) == "MThd") {
      break;
    }
  }
  if (pos == len) {
    header = 0;
    return; // failure
  }
  header = new MidiMThd(src.mid(pos, 8 + baToLong(src.mid(pos + 4, 4))));
  for (i = 0; i < header->tracks; i++) {
    for (; pos < len; pos++) {
      if (src.mid(pos, 4) == "MTrk") {
        break;
      }
    }
    if (pos == len) {
      delete header;
      header = 0;
      return; // failure
    }
    trk = src.mid(pos, 8 + baToLong(src.mid(pos + 4, 4)));
    pos += trk.length();
    tracks << new MidiMTrk(trk);
  }
}

void MidiFile::write(QIODevice* dest)
{
  bool mustClose = false;
  if (!dest->isOpen()) {
    mustClose = true;
    dest->open(QIODevice::WriteOnly);
  }
  header->tracks = tracks.count();
  dest->write(header->chunk());
  for (int i = 0; i < header->tracks; i++) {
    dest->write(tracks.at(i)->chunk());
  }
  if (mustClose) {
    dest->close();
  }
}

MidiMeta* MidiFile::findConductorEvent(uint8_t metaType) const
{
  if (tracks.length() == 0) {
    return nullptr;
  }
  MidiMTrk* track = tracks[0];
  int ct = track->events.length();
  for (int i = 0; i < ct; i++) {
    MidiEvent* event = track->events[i];
    if (event->message() != 0xF0 || event->channel != 15) {
      continue;
    }
    MidiMeta* meta = static_cast<MidiMeta*>(event);
    if (meta->eventType() == metaType) {
      return meta;
    }
  }
  return nullptr;
}

MidiText* MidiFile::text() const
{
  return static_cast<MidiText*>(findConductorEvent(MidiMeta::Text));
}

void MidiFile::setText(const QString& text)
{
  if (tracks.length() == 0) {
    return;
  }
  MidiText* event = this->text();
  if (!event) {
    event = new MidiText(MidiMeta::Text, text);
    tracks[0]->events.append(event);
  } else {
    event->setText(text);
  }
}

MidiText* MidiFile::copyright() const
{
  return static_cast<MidiText*>(findConductorEvent(MidiMeta::Copyright));
}

void MidiFile::setCopyright(const QString& text)
{
  if (tracks.length() == 0) {
    return;
  }
  MidiText* event = copyright();
  if (!event) {
    event = new MidiText(MidiMeta::Copyright, text);
    tracks[0]->events.insert(0, event);
  } else {
    event->setText(text);
  }
}

MidiText* MidiFile::sequenceName() const
{
  return static_cast<MidiText*>(findConductorEvent(MidiMeta::TrackName));
}

void MidiFile::setSequenceName(const QString& text)
{
  if (tracks.length() == 0) {
    return;
  }
  MidiText* event = sequenceName();
  if (!event) {
    event = new MidiText(MidiMeta::TrackName, text);
    tracks[0]->events.append(event);
  } else {
    event->setText(text);
  }
}
