#include <QResizeEvent>
#include <QPaintEvent>
#include <QPalette>
#include <QPainter>
#include <QBrush>
#include <QColor>
#include <QScrollBar>
#include "pianorollview.h"
#include "midichunk.h"
#include "midievent.h"

#include <QDebug>

#define NO(a) static_cast<MidiNote*>(a)

PianoRollView::PianoRollView(QWidget* parent, MidiMTrk* trk, int16_t resolution) : QAbstractScrollArea(parent)
{
  track = trk;
  numBeats = 4;
  setDivision(resolution);
  setZoom(resolution / 16);
  QPalette p = viewport()->palette();
  p.setColor(QPalette::Background, Qt::white);
  viewport()->setPalette(p);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  horizontalScrollBar()->setMinimum(0);
  horizontalScrollBar()->setMaximum(track->events.last()->offset);
  verticalScrollBar()->setMinimum(0);
  verticalScrollBar()->setMaximum(127);
  verticalScrollBar()->setValue(64);
}

PianoRollView::~PianoRollView()
{
}

uint16_t PianoRollView::getDivision() const
{
  return res;
}

int8_t PianoRollView::getFPS() const
{
  return res >> 8;
}

uint8_t PianoRollView::getResolution() const
{
  return res & 0xFF;
}

void PianoRollView::setDivision(uint16_t ppqn)
{
  res = ppqn;
  horizontalScrollBar()->setSingleStep(res * numBeats);
}

void PianoRollView::setDivision(int8_t fps, uint8_t resolution)
{
  res = fps << (8 + resolution);
}

uint16_t PianoRollView::zoom() const
{
  return zoomlevel;
}

void PianoRollView::setZoom(uint16_t level)
{
  zoomlevel = level;
}

void PianoRollView::paintEvent(QPaintEvent* event)
{
#define DY 5
  QPainter p(viewport());
  unsigned int w = event->rect().width(), pos = horizontalScrollBar()->value() + event->rect().left();
  unsigned int i, c = track->events.count();
  int top = -(verticalScrollBar()->value() * DY);
  MidiEvent *ev;

  p.setPen(QPen(QColor(192, 192, 192)));
  p.setBrush(QBrush(QColor(224, 224, 224)));
  unsigned int h = viewport()->height();
  for (i = 0; i < h; i += DY) {
    if (noteColor((top + i) / DY)) {
      p.drawRect(-1, i, w + 2, DY);
    } else {
      p.drawLine(0, i, w, i);
    }
  }

  int left = event->rect().left() - (pos % res), step = res / zoomlevel;
  if (left < 0) {
    left += res;
  }
  if (step > 1) {
    for (i = left / zoomlevel; i < w; i += step) {
      p.drawLine(i, 0, i, h);
    }
  }
  step = res / zoomlevel * numBeats;
  if (step > 1) {
    p.setPen(QPen(QColor(128, 128, 128)));
    for (i = left / zoomlevel; i < w; i += step) {
      p.drawLine(i, 0, i, h);
    }
  }

  p.setPen(QPen(QColor(0, 0, 0)));
  p.setBrush(QBrush(QColor(50, 200, 50)));

  for (i = 0; i < c; i++) {
    ev = track->events.at(i);
    if (ev->message() == 0x90 && (ev->offset >= pos || ev->offset <= pos + w)) {
      p.drawRect((ev->offset - pos) / zoomlevel, top + (127 - NO(ev)->note) * DY, NO(ev)->length / zoomlevel, DY);
    }
  }
}

void PianoRollView::resizeEvent(QResizeEvent* event)
{
  QWidget::resizeEvent(event);
  horizontalScrollBar()->setPageStep(int(event->size().width()*.75 * zoomlevel));
  verticalScrollBar()->setPageStep(int(event->size().height() / 7 * zoomlevel));
  verticalScrollBar()->setMaximum(127 - (event->size().height() / 5));
  if (verticalScrollBar()->value() > verticalScrollBar()->maximum()) {
    verticalScrollBar()->setValue(verticalScrollBar()->maximum());
  }
}

