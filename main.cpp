#include <QApplication>
#include <QFile>
#include <QVBoxLayout>
#include <QScrollBar>
#include "midifile.h"
#include "midichunk.h"
#include "midievent.h"
#include "pianorollview.h"
#include "staffview.h"
#include <QtDebug>

int main(int argc, char** argv)
{
  QApplication a(argc, argv);
  QWidget w;
  MidiFile* mf = nullptr;
  if (argc >= 2) {
    QFile f(argv[1]);
    f.open(QIODevice::ReadOnly);
    mf = new MidiFile(&f);
    f.close();

    QVBoxLayout* layout = new QVBoxLayout(&w);
    PianoRollView* s;
    uint32_t max = 0;
    QScrollBar* sb = new QScrollBar(&w);
    sb->setOrientation(Qt::Horizontal);

    for (int i = 1; i < mf->tracks.count(); i++) {
      s = new PianoRollView(&w, mf->tracks[i], mf->header->getDivision());
      s->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
      layout->addWidget(s);
      uint32_t offset = mf->tracks[i]->events.last()->offset;
      if (max < offset) {
        max = offset;
      }
      QObject::connect(sb, SIGNAL(valueChanged(int)), s->horizontalScrollBar(), SLOT(setValue(int)));
    }
    sb->setMinimum(0);
    sb->setMaximum(max);
    layout->addWidget(sb);

    w.show();

    qDebug() << (mf->sequenceName() ? mf->sequenceName()->text() : "(no sequenceName)");
    qDebug() << (mf->copyright() ? mf->copyright()->text() : "(no copyright)");
    qDebug() << (mf->text() ? mf->text()->text() : "(no text)");
  }

  StaffView sv(mf, mf ? mf->tracks[1] : nullptr);
  sv.show();

  return a.exec();
}
