#include "staffview.h"
#include "notepainter.h"
#include "midifile.h"
#include "midichunk.h"
#include "midievent.h"
#include <QScrollBar>
#include <cmath>
#include <QtDebug>

// MIDI note 0 = C-1
static const int octaveNote[]        = { 0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6 };
static const bool octaveAccidental[] = { 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0 };
// static const char* noteNames[] = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };

StaffView::StaffView(QWidget* parent) : StaffView(nullptr, nullptr, parent)
{
  // forwarded constructor only
}

StaffView::StaffView(MidiFile* file, QWidget* parent) : QAbstractScrollArea(parent), midiFile(file)
{
  setAutoFillBackground(true);
  setBackgroundRole(QPalette::Base);
  setScale(20);
  resize(500, 500);
  if (file) {
    int32_t songLength = 0;
    // TODO: separate conductor track if needed
    for (MidiMTrk* track : file->tracks) {
      MidiEvent* lastEvent = track->events.last();
      MidiNote* lastNote = MidiNote::from(lastEvent);
      int32_t trackLength = lastEvent->offset + (lastNote ? lastNote->length : 0);
      if (trackLength > songLength) {
        songLength = trackLength;
      }
    }
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    horizontalScrollBar()->setMinimum(-6);
    horizontalScrollBar()->setMaximum(songLength / 24);
    horizontalScrollBar()->setValue(-6);
  }
}

void StaffView::setScale(double value)
{
  scaleFactor = value;
  QColor selectionColor = palette().color(QPalette::Highlight);
  selectionColor.setAlphaF(0.5);
  selectionPen = QPen(selectionColor, std::sqrt(scaleFactor) * .15);
  selectionPen.setJoinStyle(Qt::RoundJoin);
  selectionPen.setCapStyle(Qt::RoundCap);

  setFont(QFont("Times New Roman", -1, 75, true));
}

double StaffView::scale() const
{
  return scaleFactor;
}

void StaffView::paintEvent(QPaintEvent*)
{
  NotePainter np(viewport());
  np.scale(scaleFactor, scaleFactor);
  np.setPen(Qt::transparent);
  np.setBrush(Qt::black);
  if (!track) {
    np.drawText(QPointF(0.5, 5), "Experimentally");
    np.drawStaff(QPointF(0, 10.5), width() / scaleFactor);
    np.drawTrebleClef(QPointF(1, 10.5));
    np.drawTimeSignature(QPointF(3, 10.5), 2, 4);
    np.beginBeam();
    np.drawNote(QPointF(7, 7.5), 3, false);
    np.drawClusterHead(QPointF(7, 7.0), 3, false);
    np.drawNote(QPointF(10, 8.5), 4, false);
    np.drawNote(QPointF(12, 9.0), 4, false);
    np.drawNote(QPointF(15, 9.5), 3, false);
    np.drawNote(QPointF(18, 10.5), 3, false);
    np.endBeam();
    np.drawBar(QPointF(21, 10.5), 3);
    np.drawNote(QPointF(23, 7.5), 0, false);
    np.drawClusterHead(QPointF(23, 7.0), 0, false);
    return;
  }
  np.drawStaff(QPointF(0, 10), width() / scaleFactor);
  np.drawStaff(QPointF(0, 16), width() / scaleFactor);
  double xOffset = horizontalScrollBar()->value();
  // TODO: cache rendering offset
  double x = -xOffset;
  double xMax = (width() / scaleFactor);
  if (xOffset < 30) {
    np.drawTrebleClef(QPointF(x - 5, 10));
    np.drawTimeSignature(QPointF(x - 3, 10), 4, 4);
    np.drawTimeSignature(QPointF(x - 3, 16), 4, 4);
  }
  int32_t lastBar = int32_t((x + 3) / (4 * 8 * 24));
  inBeam = false;
  int ct = track->events.length();
  for (int i = 0; i < ct && (inBeam || x < xMax); i++) {
    MidiEvent* me = track->events[i];
    MidiNote* note = MidiNote::from(me);
    int32_t offset = me->offset;
    if (note) {
      int32_t bar = int32_t(offset / (4 * 8 * 24));
      if (bar != lastBar) {
        if (x >= 0) {
          np.drawBar(QPointF(x, 10), 0, 1);
          np.drawBar(QPointF(x, 16), 0, -1);
        }
        lastBar = bar;
        x += 2;
      }
      x += renderNote(&np, note, i, x);
    }
  }
  if (x < xMax) {
    np.drawBar(QPointF(x, 10), 2, 1);
    np.drawBar(QPointF(x, 16), 2, -1);
  }
}

QPair<uint32_t, uint8_t> StaffView::noteType(int duration) const {
  uint32_t noteType = 0;
  int typeLength = 96 * 8;
  while (duration < typeLength) {
    noteType++;
    typeLength >>= 1;
  }
  uint8_t dots = 0;
  double fraction = double(duration) / double(typeLength);
  if (std::abs(fraction - (4.0 / 3.0)) < .05) {
    // TODO: dotted triplets
    dots = 128;
    noteType--;
    typeLength = typeLength * 4.0 / 3.0;
    fraction = double(duration) / double(typeLength);
  }
  if (fraction >= 1.75) {
    dots |= 2;
  } else if (fraction >= 1.5) {
    dots |= 1;
  }

  return qMakePair(noteType, dots);
}

int32_t StaffView::beatNumber(MidiNote* note) const {
  // TODO: time signature, resolution, etc.
  return note->offset / 24 / 8 + .01;
}

double StaffView::renderNote(NotePainter* np, MidiNote* note, int index, double x) {
  int32_t beat = beatNumber(note);
  uint32_t nextOffset = note->offset + note->length;
  int32_t nextBeat = beat + 1;
  int ct = track->events.length();
  for (int j = index + 1; j < ct; j++) {
    MidiNote* next = MidiNote::from(track->events[j]);
    // Find the next note that starts after this one ends
    // TODO: this calculation could be thrown off by sustain
    if (next && next->offset >= nextOffset) {
      nextBeat = beatNumber(next);
      nextOffset = next->offset;
      break;
    }
  }
  int duration = nextOffset - note->offset;
  auto type = noteType(duration);
  if (inBeam) {
    if (type.first <= 2) {
      np->endBeam();
      inBeam = false;
    } else {
      int32_t lastBeat = 0;
      QList<MidiNote*> beamed;
      for (int j = index - 1; j >= 0; --j) {
        MidiNote* prev = MidiNote::from(track->events[j]);
        if (prev) {
          lastBeat = beatNumber(prev);
          break;
        }
      }
      if (lastBeat < beat) {
        np->endBeam();
        inBeam = false;
      }
    }
  }
  if (type.first > 2 && !inBeam && nextBeat == beat) {
    np->beginBeam();
    inBeam = true;
  }
  double widthMultiplier = type.second ? (type.second > 1 ? 1.75 : 1.5) : 1.0;
  if (type.second >= 128) {
    // Triplets
    widthMultiplier = 2.0 / 3.0;
  }
  if (x > -8) {
    int octave = int(note->note / 12) - 1;
    int pitch = note->note % 12;
    double y = 27 - (octave * 4.0 + octaveNote[pitch] * 0.5);
    if (std::abs(y - 11) < .1) {
      np->drawLedger(QPointF(x, 10), 1);
    } else if (y <= 7) {
      np->drawLedger(QPointF(x, 10), y - 6);
    } else if (y >= 17) {
      np->drawLedger(QPointF(x, 16), y - 16);
    }
    if (octaveAccidental[pitch]) {
      QFont sharpFont("Times New Roman", 17, QFont::Bold, true);
      np->drawText(QPointF(x - .7, y + .3), "#", sharpFont);
      x += 0.8;
    }
    np->drawNote(QPointF(x, y), type.first, false);
    if (type.second >= 128) {
      QFont tripletFont("Times New Roman", 13, QFont::Bold, true);
      np->drawText(QPointF(x + .05, y - 4.1), "3", tripletFont);
      type.second -= 128;
    }
    if (type.second > 0) {
      double dotX = x + (type.first == 0 ? 1.2 : 0.9);
      np->drawEllipse(QPointF(dotX, y + .2), 0.15, 0.15);
      if (type.second > 1) {
        np->drawEllipse(QPointF(dotX + .5, y + .2), 0.15, 0.15);
      }
    }
    double sustain = double(note->length) / double(duration);
    // TODO: implicit/explicit slurs
    if (sustain < .5) {
      // staccato
      np->drawEllipse(QPointF(x, y + 1), 0.15, 0.15);
    } else if (sustain > .9) {
      // tenuto -- TODO: is .9 an appropriate boundary?
      double tenutoY = y + 0.925;
      if (std::abs(tenutoY - int32_t(tenutoY + .25)) < .25 && std::abs(tenutoY - 11) > .25) {
        tenutoY = y + 1.135;
      }
      np->fillRect(QRectF(x - .5, tenutoY, 0.9, .15), np->brush());
    }
  }
  return (type.first > 3 ? 2 : (4 << (3 - type.first))) * widthMultiplier;
}

void StaffView::renderBar(double scrollX, int index, bool hidden)
{
  if (barLayout.isEmpty()) {
    barLayout << (BarLayout){ 0, 0 };
  }
  for (int i = barLayout.length() - 1; i < index; i++) {
    renderBar(i, true);
  }
  BarLayout& bar = barLayout[index];
  if (barLayout.length() == index + 1) {
    // TODO
    uint32_t barLength = 24 * 4;
    barLayout << (BarLayout){ bar.x, bar.startTime + barLength };
  }
  BarLayout& nextBar = barLayout[index + 1];
  int ct = file->tracks.length();
  QMap<uint32_t, double> timePos;
  double finalX = bar.x;
  for (int trackIndex = 1; i < ct; i++) {
    MidiMTrk* track = file->tracks[trackIndex];
    int trackCt = track->events.length();
    double x = bar.x - scrollX;
    inBeam = false;
    for (int i = 0; i < trackCt && (inBeam || x < xMax); i++) {
      MidiEvent* me = track->events[i];
      uint32_t offset = me->offset;
      if (offset < bar.startTime) {
        continue;
      } else if (offset > nextBar.startTime) {
        break;
      }
      MidiNote* note = MidiNote::from(me);
      if (note) {
        int32_t bar = int32_t(offset / (4 * 8 * 24));
        if (bar != lastBar) {
          if (x >= 0) {
          }
          lastBar = bar;
          x += 2;
        }
        x += renderNote(&np, note, i, x);
      }
    }
  }
  np.drawBar(QPointF(x, 10), 0, 1);
  np.drawBar(QPointF(x, 16), 0, -1);
}
