#ifndef SEQUENCE_NOTEPAINTER_H
#define SEQUENCE_NOTEPAINTER_H

#include <QPainter>
#include "notepaths.h"

class NotePainter : public QPainter
{
public:
  NotePainter(QPaintDevice* device);

  QFont transformFont(const QFont& original);

  void drawText(const QPointF& pos, const QString& text);
  void drawText(const QPointF& pos, const QString& text, const QFont& font);

  void drawNote(const QPointF& pos, int noteType, bool down, unsigned int dots = 0);
  void drawClusterHead(const QPointF& pos, int noteType, bool down, unsigned int dots = 0);
  void drawStaff(const QPointF& pos, double width);
  void drawLedger(const QPointF& pos, int count);
  void drawBar(const QPointF& pos, int type = 0, double extend = 0);
  void drawTrebleClef(const QPointF& pos);
  void drawTimeSignature(const QPointF& pos, int beats, int beatType);

  void beginBeam();
  void endBeam();

  void beginStroke();
  void endStroke();

private:
  void appendPath(const QPainterPath& path, const QPointF& pos);

  bool inStroke, inBeam;
  QPolygonF strokePolygon;

  QList<DrawableNote> beamNotes;
  int beamExtend;

  double fontScaledFor, fontScaleFactor;
};

#endif
