#ifndef SEQUENCE_NOTEPATHS_H
#define SEQUENCE_NOTEPATHS_H

#include <QPainterPath>

namespace NotePaths
{

extern QPainterPath trebleClef;
extern QPainterPath noteDot;

QPainterPath note(int noteType, bool down);
QPainterPath head(int headType, bool down, bool cluster);
QPainterPath flagless(int headType, double length, bool down);

}

struct DrawableNote {
  QPointF headPos;
  int noteType;
  unsigned int dotCount : 2;
  bool stemDown : 1;
  bool headReverse : 1;

  double stemLength() const;
  double stemWidth() const;
  QPointF stemTip(int extend = 0) const;
};

#endif
