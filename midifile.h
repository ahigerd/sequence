#ifndef SEQUENCE_MIDIFILE_H
#define SEQUENCE_MIDIFILE_H

#include <QList>
#include <QByteArray>
class QIODevice;
class MidiText;
class MidiMeta;
class MidiMThd;
class MidiMTrk;

class MidiFile
{
public:
  MidiFile(uint16_t format, uint16_t division);
  MidiFile(uint16_t format, int8_t fps, uint8_t resolution);
  MidiFile(const QByteArray& src);
  MidiFile(QIODevice* src);
  MidiFile(const MidiFile& other) = default;
  MidiFile(MidiFile&& other) = default;
  ~MidiFile();

  MidiFile& operator=(const MidiFile& other) = default;
  MidiFile& operator=(MidiFile&& other) = default;

  void write(QIODevice* dest);

  MidiMThd* header;
  QList<MidiMTrk*> tracks;

  MidiText* text() const;
  void setText(const QString& text);

  MidiText* copyright() const;
  void setCopyright(const QString& text);

  MidiText* sequenceName() const;
  void setSequenceName(const QString& text);

private:
  void load(const QByteArray& src);

  MidiMeta* findConductorEvent(uint8_t metaType) const;
};

#endif
