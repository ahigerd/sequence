#include "notepainter.h"
#include <QtDebug>
#include <QMap>
#include <cmath>

NotePainter::NotePainter(QPaintDevice* device) : QPainter(device), inStroke(false), inBeam(false)
{
  setRenderHint(QPainter::Antialiasing);
  fontScaledFor = 0;
}

QFont NotePainter::transformFont(const QFont& original)
{
  double scaleFactor = transform().m11();
  if (scaleFactor != fontScaledFor) {
    fontScaledFor = scaleFactor;
    QFont ptFont("Times New Roman", 1000);
    QFont pxFont("Times New Roman");
    pxFont.setPixelSize(1000);
    fontScaleFactor = (double)QFontMetrics(ptFont).ascent() / (double)QFontMetrics(pxFont).ascent() / scaleFactor;
  }
  QFont transformed = original;
  transformed.setPointSizeF(original.pointSizeF() * fontScaleFactor);
  return transformed;
}

void NotePainter::drawText(const QPointF& pos, const QString& text)
{
  drawText(pos, text, font());
}

void NotePainter::drawText(const QPointF& pos, const QString& text, const QFont& font)
{
  QPainterPath path;
  path.addText(pos, transformFont(font), text);
  drawPath(path);
}

void NotePainter::drawNote(const QPointF& pos, int noteType, bool down, unsigned int dots)
{
  if (inBeam) {
    beamNotes.push_back({ pos, noteType, dots, down, false });
    if (noteType > 4 && noteType > beamExtend) {
      beamExtend = noteType;
    }
    return;
  }

  appendPath(NotePaths::note(noteType, down), pos);
}

void NotePainter::drawClusterHead(const QPointF& pos, int noteType, bool down, unsigned int dots)
{
  if (inBeam) {
    beamNotes.push_back({ pos, noteType, dots, down, true });
    return;
  }

  appendPath(NotePaths::head(noteType, down, true), pos);
}

void NotePainter::beginStroke()
{
  strokePolygon.clear();
  inStroke = true;
}

void NotePainter::endStroke()
{
  QPainterPath path;
  path.addPolygon(transform().inverted().map(strokePolygon));
  strokePolygon.clear();
  strokePath(path, pen());
  fillPath(path, brush());
  inStroke = false;
}

void NotePainter::beginBeam()
{
  beamNotes.clear();
  inBeam = true;
  beamExtend = 0;
}

void NotePainter::endBeam()
{
  inBeam = false;
  if (beamNotes.isEmpty()) {
    return;
  }
  DrawableNote n0 = beamNotes[0];
  if (beamNotes.length() == 1) {
    drawNote(n0.headPos, n0.noteType, n0.stemDown, n0.dotCount);
    beamNotes.clear();
    return;
  }
  double stemWidth = n0.stemWidth();
  // Note: Chords must be supplied top-to-bottom, and beamed notes must
  // be supplied left-to-right with the same stem direction. It is the
  // responsibility of the caller to provide sensible note groupings.
  int last = beamNotes.length() - 1;
  DrawableNote n1 = beamNotes[last];
  // Find the top note of the last chord
  int lastTop = last;
  while (lastTop > 0 && beamNotes[lastTop - 1].headPos.x() >= n1.headPos.x()) {
    --lastTop;
    n1 = beamNotes[lastTop];
  }
  if (n1.headPos.x() == n0.headPos.x()) {
    for (const DrawableNote& note : beamNotes) {
      if (note.headReverse) {
        drawClusterHead(note.headPos, note.noteType, note.stemDown, note.dotCount);
      } else {
        drawNote(note.headPos, note.noteType, note.stemDown, note.dotCount);
      }
    }
    beamNotes.clear();
    return;
  }
  QPointF beamStart = n0.stemTip(beamExtend), beamEnd = n1.stemTip(beamExtend);
  double slope = (beamEnd.y() - beamStart.y()) / (beamEnd.x() - beamStart.x());
  double x0 = beamStart.x(), y0 = beamStart.y();
  for (int i = 0; i <= last; i++) {
    const DrawableNote& note = beamNotes[i];
    QPointF tip = note.stemTip(beamExtend);
    double y = y0 + slope * (tip.x() - x0);
    // The extra 1 term here is to allow beams to be closer to the note
    // head than flags, since beams are more compact.
    if (note.stemDown && tip.y() - 1 > y) {
      y0 += (tip.y() - 1 - y);
    } else if (!note.stemDown && tip.y() + 1 < y) {
      y0 -= (y - tip.y() - 1);
    }
  }

  int j;
  QPointF lastTip;
  for (int i = 0; i <= last;) {
    const DrawableNote& topNote = beamNotes[i];
    double x = topNote.headPos.x();
    QPointF tip = topNote.stemTip();
    double tipX = tip.x();
    double tipY = y0 + slope * (tipX - x0);
    for (j = i; j <= last && beamNotes[j].headPos.x() == x; j++) {
      const DrawableNote& note = beamNotes[j];
      double length = std::abs(tipY - note.headPos.y());
      if (note.headReverse) {
        appendPath(NotePaths::head(note.noteType, note.stemDown, true), note.headPos);
      } else {
        appendPath(NotePaths::flagless(note.noteType, length, note.stemDown), note.headPos);
      }
      // TODO: drawDots
    }
    QPointF left(tipX, tipY);
    QPointF right(tipX + stemWidth, tipY + stemWidth * slope);
    int flagsLeft = topNote.noteType - 2, flagsRight = flagsLeft;
    if (i > 0) {
      const DrawableNote& other = beamNotes[i - 1];
      left = lastTip;
      if (other.noteType < topNote.noteType) {
        flagsLeft = other.noteType - 2;
      }
    }
    if (j <= last) {
      const DrawableNote& other = beamNotes[j];
      double otherTipX = other.stemTip().x() + stemWidth;
      right = QPointF(otherTipX, y0 + slope * (otherTipX - x0));
      if (other.noteType < topNote.noteType) {
        flagsRight = other.noteType - 2;
      }
    }
    QPainterPath flagPath;
    double dy = topNote.stemDown ? -1 : 1;
    for (int k = topNote.noteType - 3; k >= 0; --k) {
      double x0, y0, x1, y1;
      if (k < flagsLeft || i == 0) {
        x0 = left.x();
        y0 = left.y() + k * dy;
      } else if (flagsLeft >= flagsRight || i == last) {
        double width = (tipX - lastTip.x()) * .5;
        x0 = tipX - width;
        y0 = tipY + k * dy - slope * width;
      } else {
        x0 = tipX;
        y0 = tipY + k * dy;
      }
      if (k < flagsRight) {
        x1 = right.x();
        y1 = right.y() + k * dy;
      } else if (i > 0 && i < last && flagsLeft >= flagsRight) {
        x1 = tipX + stemWidth;
        y1 = tipY + k * dy + slope * stemWidth;
      } else {
        double width = (right.x() - tipX - stemWidth) * .5;
        x1 = tipX + stemWidth + width;
        y1 = tipY + k * dy + slope * width;
      }
      flagPath.moveTo(x0, y0);
      flagPath.lineTo(x1, y1);
      flagPath.lineTo(x1, y1 + dy * .5);
      flagPath.lineTo(x0, y0 + dy * .5);
      flagPath.closeSubpath();
    }
    appendPath(flagPath, QPointF());
    i = j;
    lastTip = QPointF(tipX, tipY);
  }

  beamNotes.clear();
}

void NotePainter::appendPath(const QPainterPath& path, const QPointF& pos)
{
  translate(pos);
  if (inStroke) {
    for (const QPolygonF& poly : path.toSubpathPolygons(transform())) {
      strokePolygon = strokePolygon.united(poly);
    }
  } else {
    fillPath(path, brush());
  }
  translate(-pos);
}

void NotePainter::drawStaff(const QPointF& pos, double width)
{
  double x = pos.x(), y = pos.y();
  for (int i = 0; i < 5; i++) {
    fillRect(QRectF(x, y - i - 0.05, width, 0.1), brush());
  }
}

void NotePainter::drawLedger(const QPointF& pos, int count)
{
  double x = pos.x(), y = pos.y();
  int dy = 1;
  if (count < 0) {
    y -= 4.0;
    dy = -1;
    count = -count;
  }
  for (int i = 1; i <= count; i++) {
    fillRect(QRectF(x - 1.0, y + (i * dy) - 0.05, 2.0, 0.1), brush());
  }
}

void NotePainter::drawBar(const QPointF& pos, int type, double extend)
{
  double x = pos.x(), y = pos.y(), h = 4.0;
  if (extend < 0) {
    h -= extend;
  } else if (extend > 0) {
    y += extend;
    h += extend;
  }
  fillRect(QRectF(x - 0.05, y - h, 0.1, h), brush());
  if (type > 0) {
    fillRect(QRectF(x + 0.3, y - h, type & 0x01 ? 0.1 : 0.5, h), brush());
    if (type > 2) {
      drawEllipse(QPointF(x - 0.4, pos.y() - 1.5), 0.15, 0.15);
      drawEllipse(QPointF(x - 0.4, pos.y() - 2.5), 0.15, 0.15);
    }
  }
}

void NotePainter::drawTrebleClef(const QPointF& pos)
{
  appendPath(NotePaths::trebleClef, pos);
}

void NotePainter::drawTimeSignature(const QPointF& pos, int beats, int beatType)
{
  QFont tsFont = QFont(font().family(), 30, QFont::Bold, false);
  drawText(QPointF(pos.x(), pos.y() - 2), QString::number(beats), tsFont);
  drawText(pos, QString::number(beatType), tsFont);
}
