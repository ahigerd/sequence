#ifndef SEQUENCE_MIDIEVENT_H
#define SEQUENCE_MIDIEVENT_H

#include <QString>
#include <QByteArray>

inline QByteArray toVarLen(uint32_t rhs)
{
  QByteArray retval;
  if (rhs >= 0x200000) {
    retval += ((int8_t)((rhs & (0x7F << 21)) >> 21) | 0x80);
  }
  if (rhs >= 0x4000) {
    retval += ((int8_t)((rhs & (0x7F << 14)) >> 14) | 0x80);
  }
  if (rhs >= 0x80) {
    retval += ((int8_t)((rhs & (0x7F << 7)) >> 7) | 0x80);
  }
  retval += (int8_t)(rhs & 0x0000007F);
  return retval;
}

// The following routine is adapted from http://www.borg.com/~jglatt/tech/midifile/vari.htm
inline uint32_t fromVarLen(QByteArray rhs)
{
  uint32_t retval = 0, pos = 0;
  if ((retval = rhs.at(pos++)) & 0x80) {
    retval &= 0x7F;
    do {
      retval = (retval << 7) + (rhs.at(pos) & 0x7f);
    } while (rhs.at(pos++) & 0x80);
  }
  return retval;
}

inline QByteArray longToBA(uint32_t rhs)
{
  QByteArray retval(1, (uint8_t)((rhs & 0xFF000000) >> 24));
  retval += (uint8_t)((rhs & 0x00FF0000) >> 16);
  retval += (uint8_t)((rhs & 0x0000FF00) >> 8);
  retval += (uint8_t)(rhs & 0x000000FF);
  return retval;
}

inline QByteArray shortToBA(uint16_t rhs)
{
  QByteArray retval(1, (uint8_t)((rhs & 0xFF00) >> 8));
  retval += (uint8_t)(rhs & 0x00FF);
  return retval;
}

inline uint32_t baToLong(const QByteArray& rhs)
{
  return (((uint8_t)(rhs[0]) * 256 + (uint8_t)(rhs[1])) * 256 + (uint8_t)(rhs[2])) * 256 + (uint8_t)(rhs[3]);
}

inline uint16_t baToShort(const QByteArray& rhs)
{
  return (uint8_t)(rhs[0]) * 256 + (uint8_t)(rhs[1]);
}

inline bool noteColor(uint8_t note)
{
  uint8_t interval = note % 12;
  return (interval == 1 || interval == 3 || interval == 6 || interval == 8 || interval == 10);
}

class MidiEvent
{
public:
  MidiEvent(uint8_t chan, uint32_t time);
  virtual ~MidiEvent();
  virtual uint8_t message() const = 0;
  virtual QByteArray data() const = 0;

  uint8_t status() const;
  QByteArray event() const;

  uint32_t offset;
  uint8_t channel;
};

template <typename Derived, int MESSAGE>
class MidiEventBase : public MidiEvent
{
public:
  enum { Message = MESSAGE };
  MidiEventBase(uint8_t chan, uint32_t time) : MidiEvent(chan, time) {}
  virtual uint8_t message() const { return MESSAGE; }

  static Derived* from(MidiEvent* event) {
    if (event->message() == MESSAGE) {
      return static_cast<Derived*>(event);
    }
    return nullptr;
  }
};

class MidiNote : public MidiEventBase<MidiNote, 0x90>
{
public:
  MidiNote(uint8_t chan, uint8_t noteval, uint32_t time = 0, uint8_t atk = 64,
           uint32_t len = 0, uint8_t rel = 0);
  QByteArray data() const;

  uint8_t note;
  uint8_t attack;
  uint32_t length;
  uint8_t release;
};

class MidiProgChange : public MidiEventBase<MidiProgChange, 0xC0>
{
public:
  MidiProgChange(uint8_t chan, uint8_t prog, uint32_t time = 0);
  QByteArray data() const;

  uint8_t program;
};

enum MidiControllerEnum {
  mcBankSelect = 0,
  mcModWheel,
  mcBreath,
  mcPedal = 4,
  mcPortTime,
  mcData,
  mcVolume,
  mcBalance,
  mcPan = 10,
  mcExpression,
  mcEffect1,
  mcEffect2,
  mcGeneral1 = 16,
  mcGeneral2,
  mcGeneral3,
  mcGeneral4,
  mcFineOffset = 32,
  mcHold = 64,
  mcPort,
  mcSustenuto,
  mcSoft,
  mcLegato,
  mcHold2,
  mcVariation,
  mcTimbre,
  mcRelease,
  mcAttack,
  mcBrightness,
  mcControl6,
  mcControl7,
  mcControl8,
  mcControl9,
  mcControl10,
  mcButton1,
  mcButton2,
  mcButton3,
  mcButton4,
  mcEffectLevel = 91,
  mcTremolo,
  mcChorus,
  mcCeleste,
  mcPhaser,
  mcDataInc,
  mcDataDec,
  mcNonRegFine,
  mcNonReg,
  mcRegFine,
  mcReg,
  mcSoundOff = 120,
  mcControlOff,
  mcLocalKey,
  mcNotesOff,
  mcOmniOff,
  mcOmniOn,
  mcMono,
  mcPoly
};

class MidiController : public MidiEventBase<MidiController, 0xB0>
{
public:
  MidiController(uint8_t chan, uint8_t ctrl, int16_t coarseval, int8_t fineval = -1, uint32_t time = 0);
  QByteArray data() const;
  int16_t value() const;
  void setValue(int16_t value);

  uint8_t controller;
  int8_t coarse, fine;
};

class MidiAftertouch : public MidiEventBase<MidiAftertouch, 0xA0>
{
public:
  MidiAftertouch(uint8_t chan, uint8_t noteval, uint8_t press, uint32_t time = 0);
  QByteArray data() const;

  uint8_t note;
  uint8_t pressure;
};

class MidiPressure : public MidiEventBase<MidiPressure, 0xD0>
{
public:
  MidiPressure(uint8_t chan, uint8_t press, uint32_t time = 0);
  QByteArray data() const;

  uint8_t pressure;
};

class MidiPitch : public MidiEventBase<MidiPitch, 0xE0>
{
public:
  MidiPitch(uint8_t chan, uint16_t val = 0x2000, uint32_t time = 0);
  QByteArray data() const;

  uint16_t value;
};

class MidiSysex : public MidiEventBase<MidiSysex, 0xF0>
{
public:
  MidiSysex(uint8_t chan, const QByteArray& stuff, uint32_t time = 0);
  QByteArray data() const;

  QByteArray content;
};

class MidiMeta : public MidiSysex
{
public:
  enum MetaType {
    Text = 0x01,
    Copyright = 0x02,
    TrackName = 0x03,
    InstrumentName = 0x04,
    Lyric = 0x05,
    Marker = 0x06,
    CuePoint = 0x07,
    ProgramName = 0x08,
    DeviceName = 0x09,
    ChannelPrefix = 0x20,
    SetPort = 0x21,
    EndOfTrack = 0x2F,
    Tempo = 0x51,
    SMTPEOffset = 0x54,
    TimeSignature = 0x58,
    KeySignature = 0x59,
    SequencerEx = 0x7F,
  };
  MidiMeta(uint8_t evtype, const QByteArray& stuff, uint32_t time = 0);

  uint8_t eventType() const;
};

class MidiText : public MidiMeta
{
public:
  MidiText(uint8_t evtype, const QString& text, uint32_t time = 0);

  QString text() const;
  void setText(const QString& value);
};

class MidiChannelPrefix : public MidiMeta
{
public:
  MidiChannelPrefix(uint8_t channel, uint32_t time = 0);

  uint8_t channel() const;
  void setChannel(uint8_t channel);
};

class MidiSetPort : public MidiMeta
{
public:
  MidiSetPort(uint8_t port, uint32_t time = 0);

  uint8_t port() const;
  void setPort(uint8_t port);
};

class MidiTempo : public MidiMeta
{
public:
  MidiTempo(uint32_t tempo, uint32_t time = 0);

  uint32_t tempo() const;
  void setTempo(uint32_t tempo);
};

class MidiTimeSignature : public MidiMeta
{
public:
  MidiTimeSignature(uint8_t beats, uint8_t beatType, uint8_t metronome = 0xFF, uint8_t quarter32nds = 8, uint32_t time = 0);

  uint8_t beats() const;
  uint8_t beatType() const;
  void setTimeSignature(uint8_t beats, uint8_t beatType, uint8_t beatsPerClick = 1);

  uint8_t metronome() const;
  uint8_t beatsPerClick() const;
  void setMetronome(uint8_t clocksPerClick);

  uint8_t quarter32nds() const;
  void setQuarter32nds(uint8_t count);
};

#endif
