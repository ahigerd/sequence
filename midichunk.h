#ifndef SEQUENCE_MIDICHUNK_H
#define SEQUENCE_MIDICHUNK_H

#include <QByteArray>
#include <QList>
class MidiEvent;

class MidiChunk
{
public:
  virtual ~MidiChunk();
  virtual QByteArray id() const = 0;
  virtual QByteArray data() const = 0;
  QByteArray chunk() const;
};

class MidiMThd : public MidiChunk
{
public:
  MidiMThd(uint16_t fmt, uint16_t trks, uint16_t div);
  MidiMThd(uint16_t fmt, uint16_t trks, int8_t fps, uint8_t resolution);
  MidiMThd(const QByteArray& src);
  QByteArray id() const
  {
    return "MThd";
  }
  QByteArray data() const;

  uint16_t tracks;

  uint16_t getDivision() const;
  int8_t getFPS() const;
  uint8_t getResolution() const;
  void setDivision(uint16_t ppqn);
  void setDivision(int8_t fps, uint8_t resolution);

private:
  uint16_t format;
  uint16_t division;
};

class MidiMTrk : public MidiChunk
{
public:
  MidiMTrk();
  MidiMTrk(const QByteArray& src);
  ~MidiMTrk();
  QByteArray id() const
  {
    return "MTrk";
  }
  QByteArray data() const;

  QList<MidiEvent*> events;
};

#endif
